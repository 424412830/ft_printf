/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/06 11:25:37 by abinet            #+#    #+#             */
/*   Updated: 2022/12/15 12:37:22 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdarg.h>
#include <stdio.h>

int	print_arg(const char *str, va_list args)
{
	int	count;

	count = 0;
	if (*str == 'c')
		count += ft_putchar_cnt(va_arg(args, int), 1);
	else if (*str == 's')
		count += ft_putstr_cnt(va_arg(args, char *), 1);
	else if (*str == 'p')
		count += ft_putptr_cnt(va_arg(args, unsigned long), 1);
	else if (*str == 'i' || *str == 'd')
		ft_putnbr_cnt(va_arg(args, int), 1, &count);
	else if (*str == 'u')
		ft_putnbr_base_cnt(va_arg(args, unsigned int), DECIMALE, 1, &count);
	else if (*str == 'x')
		ft_putnbr_base_cnt(va_arg(args, unsigned int), HEXA_LOW, 1, &count);
	else if (*str == 'X')
		ft_putnbr_base_cnt(va_arg(args, unsigned int), HEXA_UP, 1, &count);
	else if (*str == '%')
		count += ft_putchar_cnt('%', 1);
	else
	{
		count += ft_putchar_cnt('%', 1);
		count += ft_putchar_cnt(*str, 1);
	}
	return (count);
}
