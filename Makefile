SRCS	= ft_printf.c	ft_putchar_cnt.c	ft_putstr_cnt.c	\
print_arg.c	ft_strlen.c	ft_putnbr_base_cnt.c	ft_putnbr_cnt.c\

SRCS_BONUS	=

NAME	= libftprintf.a

OBJS	=  ${SRCS:.c=.o}

CC	= gcc

FLAGS	= -Wall -Wextra -Werror

%.o: %.c
		${CC} ${FLAGS} -c $< -o ${<:.c=.o}

${NAME}:	${OBJS}
		ar rc ${NAME} ${OBJS}
		ranlib ${NAME}

all:	${NAME}

clean :
		rm -f ${OBJS}

fclean : clean
		rm -f ${NAME}

re : fclean all

.SECONDARY : $(OBJS)

.PHONY : all clean fclean re bonus so
