/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/18 16:51:41 by abinet            #+#    #+#             */
/*   Updated: 2022/12/13 16:15:30 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <unistd.h>

int	ft_putstr_cnt(char *s, int fd)
{
	int	count;

	count = 0;
	if (!s)
	{
		ft_putstr_cnt("(null)", 1);
		return (6);
	}
	while (*s)
	{
		ft_putchar_cnt(*s, fd);
		s++;
		count++;
	}
	return (count);
}
/*
int	main(int argc, char **argv)
{
	(void) argc;
	ft_putstr_fd(argv[1], 1);
	return (0);
}
*/
