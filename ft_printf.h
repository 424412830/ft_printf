/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/03 14:52:36 by abinet            #+#    #+#             */
/*   Updated: 2022/12/15 11:34:19 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# define HEXA_LOW "0123456789abcdef"
# define HEXA_UP "0123456789ABCDEF"
# define DECIMALE "0123456789"

# include <stdarg.h>
# include <stddef.h>

int		ft_printf(const char *str, ...);
int		print_arg(const char *str, va_list arg);
int		ft_putchar_cnt(char c, int fd);
int		ft_putstr_cnt(char *s, int fd);
int		ft_putptr_cnt(unsigned long n, int fd);
int		ft_putnbr_base_cnt(unsigned long n, char *base, int fd, int *count);
size_t	ft_strlen(const char *s);
int		ft_putnbr_cnt(int n, int fd, int *count);

#endif
