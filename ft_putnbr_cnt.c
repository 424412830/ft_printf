/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_cnt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/18 17:19:36 by abinet            #+#    #+#             */
/*   Updated: 2022/12/15 11:30:11 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <unistd.h>
#include <stdlib.h>

int	ft_putnbr_cnt(int n, int fd, int *count)
{
	if (n == -2147483648)
	{
		(*count) += ft_putstr_cnt("-2147483648", 1);
		return (*count);
	}
	if (n < 0)
	{
		n = n * -1;
		(*count) += ft_putchar_cnt('-', fd);
	}
	if (n > 9)
		ft_putnbr_cnt(n / 10, fd, count);
	(*count) += ft_putchar_cnt(n % 10 + 48, fd);
	return (*count);
}
/*
int	main(int argc, char **argv)
{
	(void) argc;
	(void) argv;
	ft_putnbr_fd(atoi(argv[1]), 1);
	return (0);
}*/
