/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/18 16:39:46 by abinet            #+#    #+#             */
/*   Updated: 2022/12/13 12:36:47 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <unistd.h>

int	ft_putchar_cnt(char c, int fd)
{
	write(fd, &c, 1);
	return (1);
}
/*
int	main(int argc, char **argv)
{
	(void) argc;
	ft_putchar_fd(*argv[1], 1);
	return (0);
}
*/
